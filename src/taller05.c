
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char *mesg[2] = {"Par", "impar"} ;


int n = 0;



typedef struct estTDA{
        int c;
        int *arr;
        int cant_arr;
        char *mensaje;
}est;

void mostrar_est(est *a);
void mostrar_int(int *a, int x);
int *mas(int cant);


int *mas(int cant){

        int *arr=malloc(cant*sizeof(int));
	if(arr == NULL){
                return 0;
        }
	memset(arr,0,cant*sizeof(int));
        int *b = arr;
        int i = 0;
        for(i = 0; i < cant; i++){
                b[i]  = i *10;
        }
        return &arr[0];

}

void mostrar_est(est *a){
        int i = 0;
        for(i = 0; i < n; i++){
                printf("\nEst c: %d, mensaje: %s\n",a[i].c,a[i].mensaje);
                mostrar_int(a[i].arr,a[i].cant_arr);
               	printf("\n");
	}
}

void mostrar_int(int *a,int x){
        int i = 0;
        for(i = 0; i < x; i++){
                printf("%d\n", *(a+i));
        }
}


int *fn1(int cant, int mul){
        int *buf = malloc(cant*sizeof(int));
	if(buf == NULL){
                return 0;
        }
	memset(buf,0,cant*sizeof(int));
        int i = 0;
        for(i = 0; i < cant; i++){
                buf[i] = mul*i;
        }
        return &buf[0];
 }

void fn2(int cant, est **res){
        est *data = malloc(sizeof(est *)*cant);
	if(data == NULL){
                return;
      	}
	memset(data,0,(sizeof(est *)*cant));
        int i = 0;

	int *ptr = malloc(100*cant*sizeof(int));
	if(ptr == NULL){
                return;
        }
        for(i = 0; i < cant ; i++){
		n++;
               	data[i].c = i;
		ptr=fn1(i,3);
                data[i].arr= &ptr[0];
                data[i].cant_arr = i;
               	data[i].mensaje = mesg[ i % 2 ];
        }

        *res = (est *)data;
}

int main(int argc, char **argv){

        if(argc < 2){
                printf("Uso: ./programa <numero de elemenetos>\n");
                exit(EXIT_FAILURE);
        }

        int cant = atoi(argv[1]);


        int *arr = fn1(cant, 2);
	mostrar_int(arr,cant);
        est *res =malloc(cant*sizeof(est *));
	if(res == NULL){
                return 0;
        }
        memset(res,0,cant*sizeof(est *));
	fn2(cant, &res);

        mostrar_est(res);


	int *arr2 = mas(cant);
        mostrar_int(arr2,cant);
        exit(EXIT_SUCCESS);
}
